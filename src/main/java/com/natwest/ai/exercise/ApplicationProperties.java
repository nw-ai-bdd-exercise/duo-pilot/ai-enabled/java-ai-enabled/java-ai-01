package com.natwest.ai.exercise;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {

  private static final String PROPERTIES_PATH =
      "src/main/resources/properties/com/natwest/ai/exercise/application.properties";

  private String client_id;
  private String client_secret;
  private String redirect_url;
  private String psu_username;
  private String transactions_account_id;
  private String payee_id;

  private static ApplicationProperties application_properties;

  public static ApplicationProperties getInstance() {
    if (application_properties == null) {
      application_properties = new ApplicationProperties();
    }
    return application_properties;
  }

  private ApplicationProperties() {
    try (InputStream input = new FileInputStream(PROPERTIES_PATH)) {
      Properties prop = new Properties();
      prop.load(input);

      client_id = getProperty(prop, "CLIENT_ID");
      client_secret = getProperty(prop, "CLIENT_SECRET");
      redirect_url = getProperty(prop, "REDIRECT_URL");
      psu_username = getProperty(prop, "PSU_USERNAME");
      transactions_account_id = getProperty(prop, "TRANSACTIONS_ACCOUNT_ID");
      payee_id = getProperty(prop, "PAYEE_ID");

    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  private String getProperty(Properties prop, String key) {
    return prop.getProperty(key);
  }

  public String getClientID() {
    return client_id;
  }

  public String getClientSecret() {
    return client_secret;
  }

  public String getRedirectURL() {
    return redirect_url;
  }

  public String getPSUUsername() {
    return psu_username;
  }

  public String getTransactionsAccountID() {
    return transactions_account_id;
  }

  public String getPayeeID() {
    return payee_id;
  }
}
