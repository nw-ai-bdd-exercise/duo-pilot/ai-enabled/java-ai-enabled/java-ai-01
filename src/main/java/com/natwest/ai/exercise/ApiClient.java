package com.natwest.ai.exercise;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import org.json.*;

import com.natwest.ai.exercise.ApplicationProperties;

public class ApiClient {

    private static final String BASE_URL = "https://ob.sandbox.natwest.com";

    private static HttpsURLConnection setupConnection(String url, String method, String contentType, String accessToken) throws Exception {
        URL urlObj = new URL(url);
        HttpsURLConnection conn = (HttpsURLConnection) urlObj.openConnection();
        conn.setRequestMethod(method);
        conn.setRequestProperty("Content-Type", contentType);
        if (accessToken != null) {
            conn.setRequestProperty("Authorization", "Bearer " + accessToken);
        }
        return conn;
    }

    private static String readResponse(HttpsURLConnection conn) throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        conn.disconnect();
        return content.toString();
    }

    public static String getAccessToken() throws Exception {
        String url = "https://ob.sandbox.natwest.com/token";
        String method = "POST";
        String contentType = "application/x-www-form-urlencoded";
        HttpsURLConnection conn = setupConnection(url, method, contentType, null);

        Map<String, String> parameters = new HashMap<>();
        parameters.put("client_id", ApplicationProperties.getInstance().getClientID());
        parameters.put("client_secret", ApplicationProperties.getInstance().getClientSecret());
        parameters.put("grant_type", "client_credentials");
        parameters.put("scope", "accounts");

        conn.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
        out.flush();
        out.close();

        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            throw new Exception("Access token creation failed. Response from the server was: " + responseCode);
        }

        String response = readResponse(conn);
        JSONObject jsonObject = new JSONObject(response);
        return jsonObject.getString("access_token");
    }

    public static String grantConsent(String accessToken) throws Exception {
        String url = BASE_URL + "/open-banking/v3.1/aisp/account-access-consents";
        String method = "POST";
        String contentType = "application/json";
        HttpsURLConnection conn = setupConnection(url, method, contentType, accessToken);

        conn.setRequestProperty("Host", "ob.sandbox.natwest.com");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Cache-Control", "no-cache");
        conn.setRequestProperty("Accept", "*/*");

        JSONObject payload = new JSONObject();
        payload.put("Data", new JSONObject().put("Permissions", new JSONArray(Arrays.asList("ReadAccountsDetail", "BillPayService"))));
        payload.put("Risk", new JSONObject());

        conn.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.writeBytes(payload.toString());
        out.flush();
        out.close();

        int responseCode = conn.getResponseCode();
        if (responseCode != 201) {
            throw new Exception("Grant consent failed. Response from the server was: " + responseCode);
        }

        String response = readResponse(conn);
        JSONObject jsonObject = new JSONObject(response);
        return jsonObject.getJSONObject("Data").getString("ConsentId");
    }

    public static String authorizeConsent(String consentId) throws Exception {
        String clientId = ApplicationProperties.getInstance().getClientID();
        String encodedRedirectUrl = URLEncoder.encode(ApplicationProperties.getInstance().getRedirectURL(), "UTF-8");
        String psuUsername = ApplicationProperties.getInstance().getPSUUsername();

        String url = "https://api.sandbox.natwest.com/authorize?client_id=" + clientId + "&response_type=code id_token&scope=openid accounts&redirect_uri=" + encodedRedirectUrl + "&state=ABC&request=" + consentId + "&authorization_mode=AUTO_POSTMAN&authorization_username=" + psuUsername;
        String method = "GET";
        String contentType = "application/json";
        HttpsURLConnection conn = setupConnection(url, method, contentType, null);

        conn.setRequestProperty("Host", "ob.sandbox.natwest.com");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Accept", "*/*");

        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            throw new Exception("Authorize consent failed. Response from the server was: " + responseCode);
        }

        String response = readResponse(conn);
        JSONObject jsonObject = new JSONObject(response);
        String redirectUri = jsonObject.getString("redirectUri");
        return redirectUri.split("#")[1].split("&")[0].replace("code=", "");
    }

    public static String exchangeAuthorizationCodeForAccessToken(String authorizationCode) throws Exception {
        String url = "https://ob.sandbox.natwest.com/token";
        String method = "POST";
        String contentType = "application/x-www-form-urlencoded";
        HttpsURLConnection conn = setupConnection(url, method, contentType, null);

        conn.setRequestProperty("Host", "ob.sandbox.natwest.com");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Accept", "*/*");

        Map<String, String> parameters = new HashMap<>();
        parameters.put("grant_type", "authorization_code");
        parameters.put("client_id", ApplicationProperties.getInstance().getClientID());
        parameters.put("client_secret", ApplicationProperties.getInstance().getClientSecret());
        parameters.put("redirect_uri", ApplicationProperties.getInstance().getRedirectURL());
        parameters.put("code", authorizationCode);

        conn.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
        out.flush();
        out.close();

        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            throw new Exception("Exchange authorization code for access token failed. Response from the server was: " + responseCode);
        }

        String response = readResponse(conn);
        JSONObject jsonObject = new JSONObject(response);
        return jsonObject.getString("access_token");
    }

    public static JSONObject getAccounts(String authorizedAccessToken) throws Exception {
        String url = "https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts";
        String method = "GET";
        String contentType = "application/json";
        HttpsURLConnection conn = setupConnection(url, method, contentType, authorizedAccessToken);

        conn.setRequestProperty("Host", "ob.sandbox.natwest.com");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Cache-Control", "no-cache");
        conn.setRequestProperty("Accept", "*/*");

        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            throw new Exception("Get accounts failed. Response from the server was: " + responseCode);
        }

        String response = readResponse(conn);
        JSONObject jsonObject = new JSONObject(response);
        return jsonObject;
    }

    public static JSONObject getPayees(String authorizedAccessToken) throws Exception {
        String url = BASE_URL + "/open-banking/v3.1/aisp/accounts/" + ApplicationProperties.getInstance().getTransactionsAccountID() + "/payees";
        String method = "GET";
        String contentType = "application/json";
        HttpsURLConnection conn = setupConnection(url, method, contentType, authorizedAccessToken);

        conn.setRequestProperty("Host", "ob.sandbox.natwest.com");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Cache-Control", "no-cache");
        conn.setRequestProperty("Accept", "*/*");

        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            throw new Exception("Get payees failed. Response from the server was: " + responseCode);
        }

        String response = readResponse(conn);
        JSONObject jsonObject = new JSONObject(response);
        return jsonObject;
    }

public static JSONObject billPay(String authorizedAccessToken) throws Exception {
    String url = BASE_URL + "/open-banking/v3.1/aisp/accounts/" + ApplicationProperties.getInstance().getTransactionsAccountID() + "/bill-pay-request";
    String method = "POST";
    String contentType = "application/json";
    HttpsURLConnection conn = setupConnection(url, method, contentType, authorizedAccessToken);

    conn.setRequestProperty("Host", "ob.sandbox.natwest.com");
    conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
    conn.setRequestProperty("Connection", "keep-alive");
    conn.setRequestProperty("Accept", "*/*");

    JSONObject payload = new JSONObject();
    payload.put("payeeId", ApplicationProperties.getInstance().getPayeeID());
    payload.put("amount", "17.93");

    conn.setDoOutput(true);
    DataOutputStream out = new DataOutputStream(conn.getOutputStream());
    out.writeBytes(payload.toString());
    out.flush();
    out.close();

    int responseCode = conn.getResponseCode();
    if (responseCode != 200) {
        throw new Exception("Bill pay failed. Response from the server was: " + responseCode);
    }

    String response = readResponse(conn);
    JSONObject jsonObject = new JSONObject(response);
    return jsonObject;
}

    public static class ParameterStringBuilder {
        public static String getParamsString(Map<String, String> params)
          throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();

            for (Map.Entry<String, String> entry : params.entrySet()) {
              result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
              result.append("=");
              result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
              result.append("&");
            }

            String resultString = result.toString();
            return resultString.length() > 0
              ? resultString.substring(0, resultString.length() - 1)
              : resultString;
        }
    }

}
