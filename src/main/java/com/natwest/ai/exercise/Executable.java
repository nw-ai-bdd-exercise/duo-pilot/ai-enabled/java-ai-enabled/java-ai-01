package com.natwest.ai.exercise;

import org.json.*;

import com.natwest.ai.exercise.ApiClient;

public class Executable {

  public Executable() {
    // Default constructor.
  }

  public static void main(final String[] args) {

    System.out.println("Starting AI BDD exercise solution...");

    try {
      String accessToken2 = processAuthorization();

      System.out.println("Getting accounts...");
      JSONObject accounts = ApiClient.getAccounts(accessToken2);
      System.out.println("Accounts: " + accounts.toString());

      System.out.println("Getting payees...");
      JSONObject payees = ApiClient.getPayees(accessToken2);
      System.out.println("Payees: " + payees.toString());

      System.out.println("Paying a payee...");
      JSONObject payment_reference = ApiClient.getPayees(accessToken2);
      System.out.println("Payment: " + payment_reference.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static String processAuthorization() throws Exception {
    System.out.println("Getting access token...");
    String accessToken = ApiClient.getAccessToken();
    System.out.println("Access token: " + accessToken);

    System.out.println("Granting consent...");
    String consentId = ApiClient.grantConsent(accessToken);
    System.out.println("Consent ID: " + consentId);

    System.out.println("Authorizing consent...");
    String authorizationCode = ApiClient.authorizeConsent(consentId);
    System.out.println("Authorization code: " + authorizationCode);

    System.out.println("Exchanging authorization code for access token...");
    String accessToken2 = ApiClient.exchangeAuthorizationCodeForAccessToken(authorizationCode);
    System.out.println("Access token: " + accessToken2);

    return accessToken2;
  }
}
