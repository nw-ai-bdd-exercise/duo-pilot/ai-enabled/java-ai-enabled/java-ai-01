package com.natwest.ai.exercise.cucumber;

import io.cucumber.java.en.*;
import static org.junit.jupiter.api.Assertions.*;
import org.json.*;

import com.natwest.ai.exercise.ApiClient;

public class BillPayStepDefinitions {

  private String accessToken;
  private String consentId;
  private String authorizationCode;
  private String accessToken2;
  private JSONObject accounts;
  private JSONObject payees;
  private JSONObject payment_reference;

  public BillPayStepDefinitions() throws Exception {
    setup();
  }

  public void setup() throws Exception {
        accessToken = ApiClient.getAccessToken();
        consentId = ApiClient.grantConsent(accessToken);
        authorizationCode = ApiClient.authorizeConsent(consentId);
        accessToken2 = ApiClient.exchangeAuthorizationCodeForAccessToken(authorizationCode);
    }


  @Given("I have a valid API access token with ReadAccountsDetail API consent.")
  public void iHaveAValidAPIAccessTokenWithReadAccountsDetailAPIConsent() {
    assertNotNull(accessToken);
    assertFalse(accessToken.isEmpty());
  }


  @When("I call the accounts API endpoint.")
  public void iCallTheAccountsAPIEndpoint() throws Exception {
    accounts = ApiClient.getAccounts(accessToken2);
  }


  @Then("I receive a list of all accounts included in my Bank of APIs test data.")
  public void iReceiveAListOfAllAccountsIncludedInMyBankOfAPIsTestData() throws Exception {
    assertNotNull(accounts);
    assertFalse(accounts.toString().isEmpty());
  }


  @Given("I have a valid API access token with BillPayService API consent.")
  public void iHaveAValidAPIAccessTokenWithBillPayServiceAPIConsent() {
    assertNotNull(accessToken);
    assertFalse(accessToken.isEmpty());
  }


  @When("I call the payees API endpoint providing a valid account ID.")
  public void iCallThePayeesAPIEndpointProvidingAValidAccountID() throws Exception {
    payees = ApiClient.getPayees(accessToken2);
  }


  @Then("I receive a list of all payees associated with that account ID.")
  public void iReceiveAListOfAllPayeesAssociatedWithThatAccountID() {
    assertNotNull(payees);
    assertFalse(payees.toString().isEmpty());
  }


  @When("I call the bill-pay-request API endpoint providing a valid payeeId ID and amount.")
  public void iCallTheBillPayRequestAPIEndpointProvidingAValidPayeeIdIDAndAmount() throws Exception {
    payment_reference = ApiClient.getPayees(accessToken2);
  }


  @Then("I receive a response containing a valid request referenceNumber.")
  public void iReceiveAResponseContainingAValidRequestReferenceNumber() {
    assertNotNull(payment_reference);
    assertFalse(payment_reference.toString().isEmpty());
  }

}
