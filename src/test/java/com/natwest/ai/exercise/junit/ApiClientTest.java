package com.natwest.ai.exercise.junit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.json.*;

import com.natwest.ai.exercise.ApiClient;

public class ApiClientTest {
    private String accessToken;
    private String consentId;
    private String authorizationCode;
    private String accessToken2;

    @BeforeEach
    public void setup() throws Exception {
        accessToken = ApiClient.getAccessToken();
        consentId = ApiClient.grantConsent(accessToken);
        authorizationCode = ApiClient.authorizeConsent(consentId);
        accessToken2 = ApiClient.exchangeAuthorizationCodeForAccessToken(authorizationCode);
    }

    @Test
    public void testGetAccessToken() throws Exception {
        assertNotNull(accessToken);
        assertFalse(accessToken.isEmpty());
    }

    @Test
    public void testGrantConsent() throws Exception {
        assertNotNull(consentId);
        assertFalse(consentId.isEmpty());
    }

    @Test
    public void testAuthorizeConsent() throws Exception {
        assertNotNull(authorizationCode);
        assertFalse(authorizationCode.isEmpty());
    }

    @Test
    public void testexchangeAuthorizationCodeForAccessToken() throws Exception {
        assertNotNull(accessToken2);
        assertFalse(accessToken2.isEmpty());
    }

    @Test
    public void testGetAccounts() throws Exception {
        JSONObject accounts = ApiClient.getAccounts(accessToken2);
        assertNotNull(accounts);
        assertFalse(accounts.toString().isEmpty());
    }

    @Test
    public void testGetPayees() throws Exception {
        JSONObject payees = ApiClient.getPayees(accessToken2);
        assertNotNull(payees);
        assertFalse(payees.toString().isEmpty());
    }

    @Test
    public void testBillPay() throws Exception {
        JSONObject payment_reference = ApiClient.getPayees(accessToken2);
        assertNotNull(payment_reference);
        assertFalse(payment_reference.toString().isEmpty());
    }
}
