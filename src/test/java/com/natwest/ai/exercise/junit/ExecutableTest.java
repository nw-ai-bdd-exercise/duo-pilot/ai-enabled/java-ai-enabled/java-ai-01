package com.natwest.ai.exercise.junit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.natwest.ai.exercise.Executable;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import org.junit.jupiter.api.Test;

/** Unit tests for Executable class. */
import com.natwest.ai.exercise.Executable;

class ExecutableTest {

  @Test
  void contructorTest() {

    assertNotNull(new Executable(), "Executable constructor returned Null");
  }

}
