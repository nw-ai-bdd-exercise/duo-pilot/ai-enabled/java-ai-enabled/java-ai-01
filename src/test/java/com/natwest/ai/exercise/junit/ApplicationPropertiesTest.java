package com.natwest.ai.exercise.junit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

import com.natwest.ai.exercise.ApplicationProperties;

class ApplicationPropertiesTest {

  @Test
    void contructorTest() {

        assertNotNull(ApplicationProperties.getInstance(), "ApplicationProperties constructor returned Null");
    }

    @Test
    void getClientIDTest() {

        assertFalse(ApplicationProperties.getInstance().getClientID().isEmpty(), "getClientID returned empty string");
    }

    @Test
    void getClientSecretTest() {

        assertFalse(ApplicationProperties.getInstance().getClientSecret().isEmpty(), "getClientSecret returned empty string");
    }

    @Test
    void getRedirectURLTest() {

        assertFalse(ApplicationProperties.getInstance().getRedirectURL().isEmpty(), "getRedirectURL returned empty string");
    }

    @Test
    void getPSUUsernameTest() {

        assertFalse(ApplicationProperties.getInstance().getPSUUsername().isEmpty(), "getPSUUsername returned empty string");
    }

    @Test
    void getTransactionsAccountIDTest() {

        assertFalse(ApplicationProperties.getInstance().getTransactionsAccountID().isEmpty(), "getTransactionsAccountID returned empty string");
    }

    @Test
    void getPayeeIDTest() {

        assertFalse(ApplicationProperties.getInstance().getPayeeID().isEmpty(), "getPayeeID returned empty string");
    }


}
